var fs = require('fs');

/**
 * SYNC
 * @param path
 * @param callback
 * @returns {Array}
 */
var recursivelyFindFilesMatching = function recursivelyFindFilesMatching(path, callback) {
  var pathsToCheck = [path];
  var filesFound = [];

  //console.log("Searching " + path + slash + "*");
  while (pathsToCheck.length > 0) {
    var curr = pathsToCheck.shift();
    var relativeFilePaths = fs.readdirSync(curr);
    for (var index in relativeFilePaths) {
      if (relativeFilePaths.hasOwnProperty(index)) {
        var filePath = relativeFilePaths[index];
        var fullPath = curr + '/' + filePath;
        //console.log("fs.statSync(" + fullPath + ")");
        var stat = fs.statSync(fullPath);
        if (stat.isDirectory()) {
          pathsToCheck.push(fullPath);
        } else if (callback(filePath)) {
          filesFound.push(fullPath);
        }
      }
    }
  }
  return filesFound;
};


module.exports.recursivelyFindFilesMatching = recursivelyFindFilesMatching;