
var TemplateCompiler = require('./TemplateCompiler');

var root = "/t/private/reef";

var directory = root + "/web/dust";
var destination = root + "/web/js/templates.js";
var compiler = new TemplateCompiler(directory, destination);

compiler.compile();
