var fs   = require('fs');
var path = require('path');
var dust = require('dustjs-linkedin');
var files = require('./files');
//require('dustjs-helpers');

/**
 *
 * @param {String} directory
 * @param {String}        destination
 * @constructor
 */
var TemplateCompiler = function TemplateCompiler(directory, destination) {

  var compiledTemplates = {};


  this.compile = function compile() {
    var templateFiles = files.recursivelyFindFilesMatching(directory, function(file) {
      return file.match(/dust$/);
    });

    console.log("Found " + templateFiles.length + " templates");

    for (var index in templateFiles) {
      if (templateFiles.hasOwnProperty(index)) {

        var file = templateFiles[index];
        var data = fs.readFileSync(file, 'utf8');
        var name = getTemplateName(file);
        compiledTemplates[name] = compileTemplate(name, data);
      }
    }
    compileCompleted();
  };

  function compileTemplate(name, contents) {
    return dust.compile(contents, name)
  }


  /**
   * @param   {string} file
   * @returns {string}
   */
  function getTemplateName(file) {
    var templateName = "";
    var index = file.indexOf(directory);
    if (index >= 0) {
      templateName = file.substr(index + directory.length);           /* remove the leading prefix */
      templateName = templateName.substr(1, templateName.length - 6); /* remove .dust */
      templateName = templateName.replace(/\//g, '.');                /* replace all slashes with dots */
    }
    return templateName;
  }

  /**
   * Called when all the compilations have finished
   */
  function compileCompleted() {
    console.log("All templates compiled... writing to disk");
    var result = ["Reef.templates = {}"];
    for (var templateName in compiledTemplates) {
      if (compiledTemplates.hasOwnProperty(templateName)) {
        var compiledData = compiledTemplates[templateName];
        result.push("Reef.templates[\"" + templateName + "\"] = " + compiledData + ";");
      }
    }

    var resultData = result.join("\n");

    /* sync does not matter here as there is only one execution context. lets save some indents */
    if (fs.existsSync(destination)) {
      fs.unlinkSync(destination);
    }

    var parentDirectory = path.dirname(destination);
    if (!fs.existsSync(parentDirectory)) {
      fs.mkdirSync(parentDirectory);
    }

    console.log("Saving " + resultData.length + " bytes of compiled templates to " + destination);
    /* write result to disk, this is your 1 template file */
    fs.writeFile(destination, resultData, 'utf8', function (err) {
      if (err) {
        console.log("Error writing file " + destination + ": " + err);
      } else {
        console.log("Success");
      }
    });
  }
};

module.exports = TemplateCompiler;
