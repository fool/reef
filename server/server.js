var http = require('http');
var path = require('path');
var express = require('express');
var token = require('./token');
var creds = require('./creds');

process.env.TWILIO_ACCOUNT_SID = creds.sid; 
process.env.TWILIO_AUTH_TOKEN = creds.token;

// Create Express app and HTTP Server, serve up static HTML/CSS/etc from the
// public directory
var app = express();
var staticDir = path.join(__dirname, '..', 'web');
console.log("Static dir " + staticDir);
app.use(express.static(staticDir));
var server = http.createServer(app);

// Generate a JWT token to use with the video SDK
app.get('/token', function(request, response) {
    // Generate a token for the name requested, with both "listen" and "invite"
    // permissions (the default set of permissions)
    response.send({
        token: token.generateToken(request.query.name)
    });
});

var chats = {};
app.get('/modules/videochat', function(request, response) {
  var conversation = request.query.conversation;
  var name = request.query.name;
  var members = {};
  if (chats.hasOwnProperty(conversation)) {
    members = chats[conversation];
  }

  var now = new Date().getTime(); // MS

  var thirtySecondsAgo = now - (30 * 1000);
  for (var key in members) {
    if (members.hasOwnProperty(key) && members[key] <= thirtySecondsAgo) {
      delete members[key];
    }
  }

  members[name] = now;
  chats[conversation] = members;
  console.log("Chats is now %j", chats);
  response.send({
    members: Object.keys(members)
  });
});


// Initialize the app
token.initialize(function(err) {
    // If there was an error during init, log it and fail
    if (err) return console.error(err);

    // Otherwise start up the app server
    var port = process.env.PORT || 80;
    server.listen(port, function() {
        console.log('Express server now listening on *:' + port);
    });
});

