

var builder = function builder() {
  console.log("Builder constructor");
  var domain = Reef.lib.reef.application.Conf.domain;
  var candidateLink = $('#candidateLink');
  var interviewerLink = $('#interviewerLink');

  var lastKnownCandidateState = null;
  var lastKnownInterviewerState = null;

  var lastShortenedCandidatedState = null;
  var lastShortenedInteviewerState = null;

  var candidateUpdateInterval = null;
  var interviewerUpdateInterval = null;
  var updateHighlightDuration = 2 * 1000;

  /** one time setup */
  function setUpListeners() {
    console.log("Set up listeners");
    $("body").find("input").each(function(index, elem) {
      var jElem = $(elem);
      if (!jElem.hasClass('short')) {
        console.log("Adding change event handler");

        if (jElem.attr('id') === "candidateLink") {
          jElem.on('input', updateFromCandidateLink);

        } else if (jElem.attr('id') === "interviewerLink") {
          jElem.on('input', updateFromInterviewerLink);
        } else {
          jElem.on('input', function (event) {
            updateCandidateLink();
            updateInterviewerLink();
            onUpdate();
          });
        }
      }
    });

    $('#advanced').on('click', function() {
      $('.advanced').toggle();
    });

    $('.shorten').on('click', shortenUrls);
  }

  /**
   * Ship urls off to a shortener service
   */
  function shortenUrls() {
    var candidateShortLinkInput = $('.candidateShortLinkInput');
    lastShortenedCandidatedState = candidateLink.val();
    Reef.lib.Googl.shorten(lastShortenedCandidatedState, function(url) {
      candidateShortLinkInput.val(url);
      if (lastKnownCandidateState === lastShortenedCandidatedState) {
        candidateShortLinkInput.addClass("green");
      }
    });

    var interviewerShortLinkInput = $('.interviewerShortLinkInput');
    lastShortenedInteviewerState = interviewerLink.val();
    Reef.lib.Googl.shorten(lastShortenedInteviewerState, function(url) {
      interviewerShortLinkInput.val(url);
      if (lastKnownInterviewerState === lastShortenedInteviewerState) {
        interviewerShortLinkInput.addClass("green");
      }
    });
  }

  /**
   * any time an update happens update whether the short links are still valid
   */
  function onUpdate() {
    lastKnownCandidateState = candidateLink.val();
    $('.candidateShortLinkInput').toggleClass("green", lastKnownCandidateState === lastShortenedCandidatedState);

    lastKnownInterviewerState = interviewerLink.val();
    $('.interviewerShortLinkInput').toggleClass("green", lastKnownInterviewerState === lastShortenedInteviewerState);
  }

  /**
   * Update from a canndidate url
   */
  function updateFromCandidateLink() {
    var url = candidateLink.val();
    var justParam = url.substring(domain.length + "/?options=".length);
    var data = JSON.parse(Reef.lib.base64.decode(justParam));
    updateFromData(data);
  }

  /**
   * Update from an interviewer URL
   */
  function updateFromInterviewerLink() {
    var url = interviewerLink.val();
    var justParam = url.substring(domain.length + "/?options=".length);
    var data = JSON.parse(Reef.lib.base64.decode(justParam));
    updateFromData(data);
  }

  /**
   * Update from a URL
   * @param data
   */
  function updateFromData(data) {
    console.log("Updating from this data: " + JSON.stringify(data));
    Reef.lib.forEach(data.modules, function(m) {
      switch (m.name) {
        case "VideoChatCaller":
          $("#interviewerTwilioName").val(m.options.name);
          $("#twilioRoomName").val(m.options.conversationName);
          break;
        case "VideoChatListener":
          $("#candidateTwilioName").val(m.options.name);
          $("#twilioRoomName").val(m.options.conversationName);
          break;
        case "InterviewInfoCandidate":
          $("#candidateName").val(m.options.candidateName);
          $("#candidatePosition").val(m.options.candidatePosition);
          $("#interviewerName").val(m.options.interviewerName);
          $("#interviewTime").val(m.options.interviewTime);
          $("#recruiterName").val(m.options.recruiterName);
          break;
        case "Coderpad":
          $("#coderpadUrl").val(m.options.url);
          break;
        case "Greenhouse":
          $("#greenhouseUrl").val(m.options.url);
          break;
      }
    });
    onUpdate();
  }

  /**
   * Read page and make a new candidate link
   */
  function updateCandidateLink() {
    var options = {
      modules: [
        {
          name: "VideoChat",
          options: {
            name: $("#candidateTwilioName").val(),
            conversationName: $("#roomTwilioName").val(),
            role: "Listener"
          }
        }, {
          name: "InterviewInfoCandidate",
          options: {
            candidateName: $("#candidateName").val(),
            candidatePosition: $("#candidatePosition").val(),
            interviewerName: $("#interviewerName").val(),
            interviewTime: $("#interviewTime").val(),
            recruiterName: $("#recruiterName").val()
          }
        }, {
          name: "Coderpad",
          options: {
            url: $("#coderpadUrl").val()
          }
        }
      ]
    };
    var url = domain + "/?options=" + Reef.lib.base64.encode(JSON.stringify(options));
    candidateLink.val(url);

    if (candidateUpdateInterval) {
      window.clearTimeout(candidateUpdateInterval);
    }
    candidateLink.addClass("highlight");
    candidateUpdateInterval = window.setTimeout(function() {
      candidateLink.removeClass("highlight");
    }, updateHighlightDuration);
  }

  /**
   * Read page and make a new interviewer link
   */
  function updateInterviewerLink() {
    var options = {
      modules: [
        {
          name: "VideoChat",
          options: {
            name: $("#interviewerTwilioName").val(),
            conversationName: $("#roomTwilioName").val(),
            role: "Caller"
          }
        }, {
          name: "Coderpad",
          options: {
            url: $("#coderpadUrl").val()
          }
        }, {
          name: "InterviewInfoCandidate",
          options: {
            candidateName: $("#candidateName").val(),
            candidatePosition: $("#candidatePosition").val(),
            interviewerName: $("#interviewerName").val(),
            interviewTime: $("#interviewTime").val(),
            recruiterName: $("#recruiterName").val()
          }
        }
        /*, {
          name: "Greenhouse",
          options: {
            url: $("#greenhouseUrl").val()
          }
        }*/
      ]
    };
    var url = domain + "/?options=" + Reef.lib.base64.encode(JSON.stringify(options));
    interviewerLink.val(url);

    if (interviewerUpdateInterval) {
      window.clearTimeout(interviewerUpdateInterval);
    }
    interviewerLink.addClass("highlight");
    interviewerUpdateInterval = window.setTimeout(function() {
      interviewerLink.removeClass("highlight");
    }, updateHighlightDuration);
  }

  setUpListeners();
};
