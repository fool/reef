/**
 * base 64
 * @type {{encode: Function, decode: Function}}
 */
Reef.lib.base64 = {
  encode: function(string) {
    return encodeURIComponent(btoa(string));
  },

  decode: function(string) {
    return atob(decodeURIComponent(string));
  }
};
