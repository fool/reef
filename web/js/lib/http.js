Reef.lib.http = {
  getParameters: function getParameters() {
    var getParameters = {};
    location.search.substr(1).split("&").forEach(function(item) {
      var chunks = item.split("=");
      if (chunks.length === 2) {
        getParameters[chunks[0]] = chunks[1];
      }
    });
    return getParameters;
  }
};
