
/**
 * @param collection
 * @param callable
 * @return void
 */
Reef.lib.forEach = function forEach(collection, callable) {
  for (var key in collection) {
    if (collection.hasOwnProperty(key)) {
      callable(collection[key], key);
    }
  }
};

/**
 * @param collection
 * @param callable
 * @returns {object}
 */
Reef.lib.map = function map(collection, callable) {
  var newCollection = {};
  for (var key in collection) {
    if (collection.hasOwnProperty(key)) {
      newCollection[key] = callable(collection[key], key);
    }
  }
  return newCollection;
};

/**
 * @param collection
 * @param callable
 * @returns {Array}
 */
Reef.lib.mapA = function map(collection, callable) {
  var newCollection = [];
  Reef.lib.forEach(collection, function(item, key) {
    newCollection.push(callable(item, key));
  });
  return newCollection;
};

/**
 * Get the first item in the collection that satisfies a predicate.
 *
 *   The callback function must return a non-undefined object in case of success.
 *   The callback function must return undefined in case of failure
 *
 *
 *   first() will return the first non-undefined object seen from a callback
 *   first() will return undefined when all items in collection's callabk return undefined.
 *
 * @param   {object}   collection
 * @param   {function} callable    See above for details
 */
Reef.lib.first = function first(collection, callable) {
  var ret = undefined;
  Reef.lib.forEach(collection, function(each) {
    if (!ret) {
      var result = callable(each);
      if (result !== undefined) {
        ret = result;
      }
    }
  });
  return ret;
};

/**
 * Return the first element who's callback was truthy.
 * @param collection
 * @param callable
 * @returns {mixed}
 */
Reef.lib.find = function first(collection, callable) {
  var ret = undefined;
  Reef.lib.forEach(collection, function(each) {
    if (!ret) {
      if(callable(each)) {
        ret = each;
      }
    }
  });
  return ret;
};

/**
 * Determine if any item in a collection satisfies a predicate.
 *
 * @param   {object}   collection
 * @param   {function} callable
 * @returns {boolean}  True if any item in the collection returns true.
 */
Reef.lib.contains = function contains(collection, callable) {
  var success = false;
  Reef.lib.first(collection, function(each) {
    if (callable(each)) {
      success = true;
      return true;
    } else {
      return undefined;
    }
  });
  return success;
};

/**
 * Returns true if callable returned true for all items in the collection.
 *
 * @param collection
 * @param callable
 * @returns {boolean}
 */
Reef.lib.satisfies = function satisfies(collection, callable) {
  var success = false;
  Reef.lib.first(collection, function(item) {
    if (callable(item)) {
      success = true;
      return true;
    } else {
      return undefined;
    }
  });
  return success;
};

Reef.lib.filter = function filter(collection, callable) {
  var newCollection = {};
  Reef.lib.forEach(collection, function(item, key) {
    if (callable(item)) {
      newCollection[key] = item;
    }
  });
  return newCollection;
};

/**
 *
 * @param   {object}   collection
 * @param   {function} callable
 * @returns {Array}
 */
Reef.lib.collect = function collect(collection, callable) {
  var newCollection = [];
  Reef.lib.forEach(collection, function(item) {
    var result = callable(item);
    if (result) {
      newCollection.push(result);
    }
  });
  return newCollection;
};

/**
 * @param   {object}   collection
 * @param   {function} callable that returns number
 * @returns {number}   sum
 */
Reef.lib.sum = function sum(collection, callable) {
  var sum = 0;
  Reef.lib.forEach(collection, function (value) {
    sum += callable(value);
  });
  return sum;
};


/**
 * Joins a collection with optional end caps.
 *
 * Here's how the overloads work, all 3 are valid invocations:
 *   mkString(collection, join)
 *   mkString(collection, head, join)
 *   mkString(collection, head, join, tail)
 *
 *   pay special note to the 'join' parameter jumping between the first and 2nd case
 *
 * @param   {object} collection
 * @param   {string} a1           See above for how the last 3 arguments work
 * @param   {string} [a2]         Optional
 * @param   {string} [a3]         Optional
 * @returns {string}
 */
Reef.lib.mkString = function mkString(collection, a1, a2, a3) {
  var head = "";
  var join = "";
  var tail = "";
  if (arguments.length === 2) {
    join = a1;
  } else if (arguments.length > 2) {
    head = a1;
    join = a2;
    tail = a3 || tail;
  }

  var result = head;
  Reef.lib.forEach(collection, function(item) {
    result = result + item + join;
  });
  return result + tail;
};

/**
 * Applies a callback to a collection where the callback returns array.
 * ALl the arrays will be flattened to one big array.
 *
 * @param   collection
 * @param   callback
 * @returns {Array}
 */
Reef.lib.collectFlat = function collectFlat(collection, callback) {
  var results = [];
  Reef.lib.forEach(collection, function(child) {
    var r = callback(child);
    if (r instanceof Array) {
      results.concat(r);
    } else {
      results.push(r);
    }
  });
  return results;
};

/**
 * Figures out an index that is in the array
 * @param   {Array}  collection
 * @param   {number} index
 * @returns {mixed} a value in the array
 */
Reef.lib.squeeze = function squeeze(collection, index) {
  return collection[Math.max(Math.min(index, collection.length), 0)];
};


Reef.lib.copy = function copy(collection) {
  var result = {};
  Reef.lib.forEach(collection, function(value, key) {
    result[key] = value;
  });
  return result;
};


Reef.lib.copyArray = function copy(collection) {
  var result = [];
  Reef.lib.forEach(collection, function(value, key) {
    result.push(value);
  });
  return result;
};

Reef.lib.range = function range(start, end) {
  var result = [];
  for (var i = start; i <= end; i++) {
    result.push(i);
  }
  return result;
};

/**
 * Optimization level: none
 *
 * @param {Array} list
 * @param {function} comparator This is optional, if not provided the default is triple equals ===.
 *                              The comparator function accepts two arguments, each will be items from the list.
 *                              It returns true if these items are equivalent, false otherwise.
 * @return {Array} A unique subset of the original list
 */
Reef.lib.unique = function unique(list, comparator) {
  var uniqueList = [];
  if (typeof comparator !== 'function') {
    comparator = Reef.lib.uniqueDefaultComparator;
  }
  Reef.lib.forEach(list, function(item) {
    Reef.lib.forEach(uniqueList, function(uniqueItem) {
      if (!comparator(uniqueItem, item)) {
        uniqueList.push(item);
      }
    });
  });
  return uniqueList;
};
Reef.lib.uniqueDefaultComparator = function(item1, item2) { return item1 === item2; };