/**
 * https://developers.google.com/url-shortener/v1/getting_started#shorten
 * @constructor
 */
Reef.lib.Googl = {

  /**
   * Shortens a url and gives the short url to the callback.
   * @param {string}   url
   * @param {function} callback   This should take one argument, the short url
   */
  shorten: function shorten(url, callback) {
    var body = JSON.stringify({
      longUrl: url
    });

    var googlUrl = Reef.lib.googl.URL + "?key=" + encodeURIComponent(Reef.lib.reef.application.Conf.googlKey);

    $.ajax({
      url: googlUrl,
      type: "POST",
      data: body,
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      success: function(googlResponse) {
        if (googlResponse.hasOwnProperty("error")) {
          console.log("Error from googl: " + googlResponse.error);
        }
        if (googlResponse.hasOwnProperty("id") && typeof callback === 'function') {
          callback(googlResponse.id);
        }
      }
    });
  }
};

Reef.lib.googl = {};
Reef.lib.googl.URL = "https://www.googleapis.com/urlshortener/v1/url";
