
Reef.lib.reef.application.Application = function Application(originalOptions, parentElementId) {

  var options = new Reef.lib.reef.application.Options(originalOptions);
  console.log("options is: " + JSON.stringify(options));
  console.log("options.modules is: " + JSON.stringify(options.modules));
  var modules = [];
  var parentElement = $("#" + parentElementId);
  var initialized = false;
  var buttonsContainer = null;
  var modulesContainer = null;
  var self = this;
  var activeModule = null;

  function errors(message, name) {
    console.log("Error rendering template: " + name + " " + message);
  }

  Reef.renderer = new Reef.lib.TemplateRenderer(errors);


  function initialize() {
    if (!initialized) {
      console.log("initializing now");
      initialized = true;
      render(renderAllModules);
      $(window).resize(resizeEvent);
      resizeEvent();
    }
  }


  function renderAllModules() {
    var first = true;
    var once = true;
    Reef.lib.forEach(options.modules, function (moduleOptions) {
      console.log("Creating module: " + JSON.stringify(moduleOptions));
      var module = Reef.lib.reef.modules.module.create(moduleOptions.name, moduleOptions.options);
      modules.push(module);
      var callback = null;
      if (first) {
        first = false;
        callback = function () {
          if (once) {
            once  = false;
            console.log("Activating first default open module, switchTo(" + module.id+ ")");
            self.switchTo(module);
          } else {
            console.log("Callback activate called multiple times!!");
          }
        };
      }
      renderModuleContainer(module, callback);
    });
  }

  function renderModuleContainer(module, callback) {
    console.log("Rendering a button for " + module.id);
    Reef.renderer.render(buttonsContainer, "reef.tab", {module: module.id, name: module.name}, function () {
      console.log("Button rendering for  " + module.id + " finished");
      var li = buttonsContainer.children('.' + module.id);
      var button = li.find('button');
      button.on('click', function () {
        self.switchTo(module);
      });
    }, "append");


    console.log("RenderModuleContainer(" + module.id + "), callback has type: " + (typeof callback));
    Reef.renderer.render(modulesContainer, "reef.module", {id: module.id}, function() {
      console.log("Module container rendering finished " + module.id);
      renderModule(module, callback);
    }, "append");


  }

  function renderModule(module, callback) {
    console.log("==> Rendering module: " + module.name);
    var moduleContainer = modulesContainer.children("." + module.id);
    module.render(moduleContainer, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }

  function render(callback) {
    Reef.renderer.render(parentElement, "reef.content", {}, function() {
      buttonsContainer = parentElement.find('.buttons');
      modulesContainer = parentElement.find('.modules');
      if (typeof callback === 'function') {
        callback();
      }
    });
  }

  this.switchTo = function switchTo(module, callback) {
    var callb = function() {
      var mod = Reef.lib.find(modules, function(m) {
        return module == m;
      });
      if (mod) {
        var button = buttonsContainer.find("." + mod.id);
        button.addClass("selected");
        // activate the current button in the li list
        console.log("Activating module " + module.name);
        mod.activate(null, callback);
        activeModule = mod;
      } else {
        console.log("switch to for invalid module, cant find: " + module.name);
      }
      if (typeof callback === 'function') {
        callback();
      }
    };

    if (activeModule) {
      var activeButton = buttonsContainer.find("." + activeModule.id);
      activeButton.removeClass("selected");
      console.log("Deactivating current module " + activeModule.name);
      // deactivate the current button in the li list
      activeModule.deactivate(callb);
    } else {
      callb();
    }
  };

  function resizeEvent() {
    var totalHeight = parentElement.height();
    var buttonsHeight = buttonsContainer.height();
    console.log("Total height: " + totalHeight + " buttonsHeight: " + buttonsHeight + " modules Height = " + (totalHeight - buttonsHeight));
    modulesContainer.css('max-height', (totalHeight - buttonsHeight) + 'px');
  }

  initialize();
};