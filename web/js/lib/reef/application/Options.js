

Reef.lib.reef.application.Options = function Options(options) {


  this.modules = [];
  var initialized = false;
  var self = this;
  init();

  function init() {
    if (!initialized) {
      if (options.hasOwnProperty('modules') && options.modules instanceof Array) {
        Reef.lib.forEach(options.modules, function (module) {
          if (module.hasOwnProperty('name') && typeof module.name === 'string') {
            var moduleName = module.name;
            var options = {};
            if (module.hasOwnProperty('options') && typeof module.options === 'object') {
              options = module.options;
            }
            self.modules.push({name: moduleName, options: options});
          }
        });
      } else {
        throw new Error("Missing modules");
      }

      initialized = true;
    }
  }
};
