/**
 * Modules must have two properties:
 *
 *    1. id:   A unique identifier for this module. if there's 2 instances of the same module the id should be different
 *
 *    2. name: The tab name on the top of the page
 * @constructor
 */
Reef.lib.reef.modules.Module = function Module() {
  this.render = function render(callback) {
    throw new Error("Unimplemented render()");
  };

  this.activate = function activate(options, callback) {
    throw new Error("Unimplemented activate()");
  };

  this.deactivate = function deactivate(callback) {
    throw new Error("Unimplemented deactivate()");
  };
};

Reef.lib.reef.modules.module = {};

/**
 * creates new module
 * @param   {string} name     The module class name
 * @param   {object} options  The module options
 * @returns {Reef.lib.reef.modules.Module}
 */
Reef.lib.reef.modules.module.create = function create(name, options) {
  return new Reef.modules[name](options);
};
