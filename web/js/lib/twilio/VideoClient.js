Reef.lib.twilio.VideoClient = function VideoClient(name, conversationName, meSelector, youSelector, participantConnectedCallback, participantDisconnectedCallback) {

  var endpoint = null;
  var conversation = null;
  var localMedia = null;

  function getToken(name, callback) {
    console.log("Getting token for: " + twilioName(name));
    $.getJSON('/token', {
      name: twilioName(name)
    }, function(data) {
      console.log('Token response:' + JSON.stringify(data));
      if (typeof callback == 'function') {
        callback(data.token);
      }
    });
  }

  function twilioName(someName) {
    return conversationName + "_" + someName;
  }

  // Initialize video calling app with my endpoint
  this.init = function init(callback) {
    getToken(name, function(token) {
      endpoint = new Twilio.Endpoint(token);
      console.log('Endpoint Created:');
      console.log(endpoint);

      Twilio.LocalMedia.getLocalMedia().then(function(lm) {
        localMedia = lm;
        console.log("localMedia.attach(" + meSelector +")");
        localMedia.attach(meSelector);
        $(meSelector).draggable();


        // Automatically accept any incoming calls
        endpoint.on('invite', function(invitation) {
          console.log("INVITE EVENT received, going to accept and display media");
          var options = {};
          if (localMedia) {
            options = {localMedia: localMedia};
          }
          invitation.accept(options).then(function(acceptedConversation) {
            conversation = acceptedConversation;
            setupConversation();
          });
        });

        if (typeof callback === 'function') {
          callback();
        }
      });
    });
  };

  this.listen = function listen() {
    if (endpoint) {
      console.log("listening for calls");
      endpoint.listen();
    }
  };

  this.startCall = function startCall(otherPersonsName) {
    console.log("createConversation(" + twilioName(otherPersonsName) + ")");
    var options = {};
    if (localMedia) {
      console.log("Using existing local media");
      options = {localMedia: localMedia};
    }
    var p = endpoint.createConversation(twilioName(otherPersonsName), options);
    p.then(function(createdConversation) {
      conversation = createdConversation;
      setupConversation();
    });
  };

  /** called when a conversation is made, set up event handlers here */
  function setupConversation() {
    conversation.on('participantConnected',    participantConnected);
    conversation.on('participantDisconnected', participantDisconnected);
  }

  function participantConnected(participant) {
    console.log("Showing participant, you selector: " + youSelector);
    participant.media.attach(youSelector);

    if (typeof participantConnectedCallback === 'function') {
      participantConnectedCallback(participant);
    }
  }

  function participantDisconnected(participant) {
    console.log("Participant disconnected " + JSON.stringify(participant));

    if (typeof participantDisconnectedCallback === 'function') {
      participantDisconnectedCallback(participant);
    }
  }
};
