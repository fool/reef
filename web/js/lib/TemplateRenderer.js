
/**
 * Implements templates using dust.js (linked in edition)
 * @param {function} errorCallback  A function that is called when an error occurs rendering a template
 */
Reef.lib.TemplateRenderer = function TemplateRenderer(errorCallback) {
  var defaultRenderMode = "replace";
  var renderModes = ["replace", "append"];

  Reef.lib.forEach(Reef.templates, function(template) {
    dust.loadSource(template);
  });

  /**
   *
   * @param {HTMLElement} htmlElement   The HTML element to render inside
   * @param {string}      templateName  The name of the template to render
   * @param {object}      templateData  The data to render the template with
   * @param {function}    callback      What to call when the rendering is complete
   * @param {string}      [renderMode]  Optional: How to render the template
   */
  this.render = function (htmlElement, templateName, templateData, callback, renderMode) {
    var e = htmlElement;
    if (htmlElement instanceof jQuery) {
      e = htmlElement[0];
    }
    var isFunction        = typeof renderMode === "function";
    var isStringAndInList = typeof renderMode === "string" && renderModes.indexOf(renderMode) !== -1;
    if (!isFunction && !isStringAndInList) {
      renderMode = defaultRenderMode;
    }

    dust.render(templateName, templateData, function(err, out) {
      if (err) {
        if (typeof errorCallback === "function") {
          errorCallback(err, templateName, templateData);
        }
      } else {
        renderCompiledTemplate(renderMode, e, out, callback);
      }
    });
  };

  /** sometimes it doesnt happen right away, so weird.. */
  function waitForInnerHtmlToFinish(element, callback) {
    if (element.childNodes.length > 0) {
      callback();
    } else {
      window.setTimeout(function() { waitForInnerHtmlToFinish(element, callback) }, 10);
    }
  }

  /**
   * @param {string} renderMode
   * @param {HTMLElement} htmlElement
   * @param {string} html
   * @param {function} callback
   */
  function renderCompiledTemplate(renderMode, htmlElement, html, callback) {
    if (typeof renderMode === "function") {
      renderMode(htmlElement, html);
    }
    else if (renderMode === "replace") {
      htmlElement.innerHTML = html;
    } else if (renderMode === "append") {
      var tmpHolder = document.createElement("div");
      tmpHolder.innerHTML = html;

      /*wait for innerHTML to finish */
      waitForInnerHtmlToFinish(tmpHolder, function() {
        Reef.lib.forEach(tmpHolder.childNodes, function(child) {
          if (htmlElement instanceof HTMLElement) {
            if (child instanceof Node) {
              htmlElement.appendChild(child);
            } else {
              console.log("Found a child that is not a Node, skipping " + child);
            }
          } else {
            console.log("unable to render into something thats not an html element");
          }
        });
      });
    }
    if (typeof callback === 'function') {
      callback(html);
    }
  }
};

