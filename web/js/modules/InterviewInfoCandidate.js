/**
 * Interview info for the candidate.
 * @param parentElement
 * @param {object} options Must have keys:
 *   "candidateName"
 *   "candidatePosition"
 *   "interviewerName"
 *   "interviewTime"
 *   "recruiterName"
 * @constructor
 */
Reef.modules.InterviewInfoCandidate = function InterviewInfoCandidate(options) {
  var self = this;
  this.id = 'interviewinfo';
  this.name = 'Interview Info';
  var parentElement = null;

  this.render = function render(_parentElement, callback) {
    parentElement = _parentElement;
    Reef.renderer.render(parentElement, "modules.InterviewInfoCandidate.view", options, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  };

  this.activate = function activate(options, callback) {
    parentElement.show();
    if (typeof callback === 'function') {
      callback();
    }
  };

  this.deactivate = function deactivate(callback) {
    parentElement.hide();
    if (typeof callback === 'function') {
      callback();
    }
  };
};
Reef.modules.InterviewInfoCandidate.prototype = Object.create(Reef.lib.reef.modules.Module.prototype);
