
Reef.modules.VideoChatCaller = function VideoChatCaller(parentElement, options) {

  this.id = "videochat";
  this.name = "Video Chat";
  var meSelector = "#me";
  var youSelector = "#you";
  var self = this;
  var conversation = options.conversationName;        /* the room name */
  var name = options.name;                            /* your name */
  var otherPersonsName = options.otherPersonsName;    /* not used anymore */
  var twilio = new Reef.lib.twilio.VideoClient(name, conversation, meSelector, youSelector);
  var started = false;              /* has the call started */
  var members = null;               /* jQuery element that shows the peopel in the call */
  this.pingDuration = 5 * 1000;     /* how often to ping in MS */
  var called = [];                  /* hack to prevent calling someone twice */
  var lastMembers = null;           /* Array[String] of all the names in the call */
  var me = null;                    /* jQuery element for your video */
  var you = null;                   /* jQuery element for their video */
  var youVideoContainer = null;     /* jQuery element for container that holds your video */
  var myVideoContainer = null;      /* jQuery element for container that holds their video */

  this.render = function render(callback) {
    parentElement.empty();
    parentElement.addClass("videoModule");

    youVideoContainer = $("<div></div>");
    myVideoContainer = $("<div></div>");

    var caller = $('<div class="iblock">Call members online: <span class="members"></span></div>');
    members = caller.find(".members");

    you = $('<div id="you" class="yourvideo video"></div>');
    youVideoContainer.append(caller);
    youVideoContainer.append(you);

    me = $('<div id="me" class="myvideo video"></div>');
    myVideoContainer.append(me);

    youVideoContainer.find('button').on('click', function (event) {
      console.log("twilio.startCall(" + otherPersonsName + ")");
      twilio.startCall(otherPersonsName);
      $(event.target).hide();
    });

    parentElement.append(youVideoContainer);
    parentElement.append(myVideoContainer);

    if (typeof callback === 'function') {
      callback();
    }
  };

  this.deactivate = function deactivate(callback) {
    myVideoContainer.hide();
    // move their video to the bottom left corner
    you.addClass("floating_corner_video");
    if (typeof callback === 'function') {
      callback();
    }
  };

  this.activate = function activate(options, callback) {
    parentElement.show();
    myVideoContainer.show();
    if (!started) {
      started = true;
      console.log("twilio.init()");
      twilio.init(function() {
        resetPing(0);
      });
    }

    // if thier video is in bottom left corner, move it back to regular spot
    you.removeClass("floating_corner_video");
    if (typeof callback === 'function') {
      callback();
    }
  };

  function resetPing(timeout) {
    window.setTimeout(function() {
      ping("/modules/videochat?conversation=" + conversation + "&name=" + name);
    }, timeout || self.pingDuration)
  }

  function ping(url) {
    $.get(url, function(response) {
      var memberNames = response.members;

      // response has{
      // members: ['name1', 'name2'],
      // you need to filter yourself out, but call everyone in the list.
      if (memberNames.length > 1) {
        Reef.lib.forEach(memberNames, function(member) {
          if (member !== name) {
            if (!called.hasOwnProperty(member)) {
              console.log("twilio.startCall(" + member + ")");
              twilio.startCall(member);
              called.push(member);
            }
          }
        })
      }
      if (memberNames !== lastMembers) {
        // do some fancy highlight on members like a flash green to show that it changed
        members.css('background-color', '#73FF73');
        //members.animate({"background-color": "none"}, 250);
        members.animate({backgroundColor: "white"}, 250);
        lastMembers = memberNames;
      }
      members.text(memberNames.join(", "));
      resetPing();
    });
  }
};
Reef.modules.VideoChatCaller.prototype = Object.create(Reef.lib.reef.modules.Module.prototype);
