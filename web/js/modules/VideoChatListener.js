/**
 * To simplify things a lot this video chat endpoint only listens for calls.
 * This will be the candidate in the interview.
 *
 * @param parentElement
 * @param options
 * @constructor
 */
Reef.modules.VideoChatListener = function VideoChatListener(parentElement, options) {

  this.id = "videochatlistener";
  this.name = "Video Chat";
  var meSelector = "#me";
  var youSelector = "#you";
  var self = this;
  var conversation = options.conversationName;
  var name = options.name;
  var twilio = new Reef.lib.twilio.VideoClient(name, conversation, meSelector, youSelector);
  var started = false;
  var members = null;
  this.pingDuration = 5 * 1000;
  var you = null;
  var me = null;
  var lastMembers = null;
  var youVideoContainer = null;
  var myVideoContainer = null;

  this.render = function render(callback) {
    parentElement.empty();
    parentElement.addClass("videoModule");


    youVideoContainer = $("<div></div>");
    myVideoContainer = $("<div></div>");

    var caller = $('<div class="iblock">Call members online: <span class="members"></span></div>');
    members = caller.find(".members");

    you = $('<div id="you" class="yourvideo video"></div>');
    youVideoContainer.append(caller);
    youVideoContainer.append(you);

    me = $('<div id="me" class="myvideo video"></div>');
    myVideoContainer.append(me);



    parentElement.append(youVideoContainer);
    parentElement.append(myVideoContainer);
    if (typeof callback === 'function') {
      callback();
    }
  };

  this.activate = function activate(options, callback) {
    parentElement.show();
    myVideoContainer.show();

    if (!started) {
      started = true;
      twilio.init(function() {
        console.log("Listening on twilio...");
        twilio.listen();
        resetPing(0);
      });
    }

    // if thier video is in bottom left corner, move it back to regular spot
    you.removeClass("floating_corner_video");
    if (typeof callback === 'function') {
      callback();
    }
  };

  function resetPing(timeout) {
    window.setTimeout(function() {
      ping("/modules/videochat?conversation=" + conversation + "&name=" + name);
    }, timeout || self.pingDuration)
  }

  function ping(url) {
    $.get(url, function(response) {
      var memberNames = response.members;
      if (lastMembers !== memberNames) {
        // add some fancy animation on members
        members.css('background-color', '#73FF73');
        //members.animate({"background-color": "white"}, 250);
        members.animate({ backgroundColor: "white"}, 250);
        lastMembers = memberNames;
      }
      members.text(memberNames.join(", "));
      resetPing();
    });
  }

  this.deactivate = function deactivate(callback) {
    myVideoContainer.hide();
    // move their video to the bottom left corner
    you.addClass("floating_corner_video");
    if (typeof callback === 'function') {
      callback();
    }
  };
};
Reef.modules.VideoChatListener.prototype = Object.create(Reef.lib.reef.modules.Module.prototype);