/**
 *
 * @param {jQuery Element} parentElement
 * @param {object} options
 *
 * {
 *   "conversationName": "Room Name",
 *   "name": "My Name",
 *   "role": "Caller" | "Listener"
 * }
 *
 * @constructor
 */
Reef.modules.VideoChat = function VideoChat(options) {

  this.id = "videochat";
  this.name = "Video Chat";
  var meSelector = "#me";
  var youSelector = "#you";
  var self = this;
  var conversation = options.conversationName;        /* the room name */
  var name = options.name;                            /* your name */
  var role = options.role;
  var twilio = new Reef.lib.twilio.VideoClient(name, conversation, meSelector, youSelector, participantConnected, participantDisconnected);
  var started = false;              /* has the call started */
  this.pingDuration = 1 * 1000;     /* how often to ping in MS */
  var called = [];                  /* hack to prevent calling someone twice */
  var me = null;                    /* jQuery element for your video */
  var you = null;                   /* jQuery element for their video */
  var youVideoContainer = null;     /* jQuery element for container that holds your video */
  var myVideoContainer = null;      /* jQuery element for container that holds their video */
  var parentElement = null;         /* jQuery element for the div that holds everything */
  var dots = null;                  /* jQuery element for dots */
  var callStarted = false;          /* has the call been started? */
  var draggableInitialized = false;
  var tabOpen = false;

  this.render = function render(_parentElement, callback) {
    parentElement = _parentElement;
    parentElement.addClass("videoModule");
    Reef.renderer.render(parentElement, "modules.VideoChat.chat", {}, function() {
      youVideoContainer = parentElement.find('.youcontainer');
      myVideoContainer = parentElement.find('.mecontainer');
      me = myVideoContainer.find('.myvideo');
      you = youVideoContainer.find('.yourvideo');
      dots = parentElement.find('.dots');

      if (!started) {
        started = true;
        console.log("twilio.init()");
        twilio.init(function() {
          if (role === "Listener") {
            console.log("twilio.listen()");
            twilio.listen();
          }
          resetPing(0);
        });
      }

      if (typeof callback === 'function') {
        callback();
      }

    });
  };

  /**
   * This entire module needs to be hidden except the video, you element
   * And resized to that size.
   *
   * so you add a class to parent that is like floating_corner_video but overrides all the defualt settings for width/height
   * and makes it small.
   * z-index is NOT going to work here.
   *
   * @param callback
   */
  this.deactivate = function deactivate(callback) {
    tabOpen = false;
    myVideoContainer.hide();
    // move their video to the bottom left corner
    parentElement.find('.m').hide();
    parentElement.addClass("floating_corner_video");

    if (!callStarted) {
      parentElement.hide();
    } else {
      if (!draggableInitialized) {
        parentElement.draggable();
        draggableInitialized = true;
      } else {
        parentElement.draggable('enable');
      }
    }

    //$(youSelector).draggable('enable');
    if (typeof callback === 'function') {
      callback();
    }
  };

  this.activate = function activate(options, callback) {
    tabOpen = true;
    parentElement.show();
    parentElement.find('.m').show();
    parentElement.removeClass("floating_corner_video");
    myVideoContainer.show();

    if (draggableInitialized) {
      parentElement.draggable('disable');

      parentElement.css('width', '100%');
      parentElement.css('height', '100%');
      parentElement.css('right', '');
      parentElement.css('bottom', '');
      parentElement.css('left', '');
      parentElement.css('top', '');
    }

    // if thier video is in bottom left corner, move it back to regular spot
    you.removeClass("floating_corner_video");
    if (typeof callback === 'function') {
      callback();
    }
  };

  function resetPing(timeout) {
    var url = "/modules/videochat?conversation=" + encodeURIComponent(conversation) + "&name=" + encodeURIComponent(name);
    window.setTimeout(function() {
      if (role === "Caller") {
        pingCaller(url);
      } else {
        pingListener(url);
      }
    }, timeout || self.pingDuration)
  }

  function pingCaller(url) {
    $.get(url, function(response) {
      var memberNames = response.members;
      // response has{
      // members: ['name1', 'name2'],
      // you need to filter yourself out, but call everyone in the list.

      if (!callStarted) {
        updateDots();
      }

      if (memberNames.length > 1) {
        Reef.lib.forEach(memberNames, function(member) {
          if (member !== name) {
            if (called.indexOf(member) === -1) {
              console.log("twilio.startCall(" + member + ")");
              twilio.startCall(member);
              called.push(member);
            }
          }
        })
      }
      resetPing();
    });
  }


  function pingListener(url) {
    $.get(url, function(response) {
      if (!callStarted) {
        updateDots();
      }
      resetPing();
    });
  }

  function updateDots() {
    var dotsText = dots.text();
    if (dotsText.length > 6) {
      dotsText = '';
    } else {
      dotsText += '.';
    }
    dots.text(dotsText);
  }

  function participantConnected() {
    if (!callStarted) {
      callStarted = true;
      parentElement.show(); /* if it isn't already showing */
      parentElement.find('.empty').hide();
      youVideoContainer.show();

      if (!tabOpen && !draggableInitialized) {
        parentElement.draggable();
        draggableInitialized = true;
      }
    }
  }

  function participantDisconnected(participant) {
    if (callStarted) {
      callStarted = false;
      if (!tabOpen) {
        parentElement.hide();
      }

      // this called lookup is going to fail because the address is conversation + member name and you are only storing member name
      var index = called.indexOf(participant.address);
      if (index !== -1) {
        called.splice(index, 1)
      }
      parentElement.find('.empty').show();
      youVideoContainer.hide();
    }
  }
};
Reef.modules.VideoChat.prototype = Object.create(Reef.lib.reef.modules.Module.prototype);