
Reef.modules.Greenhouse = function Greenhouse(options) {
  var self = this;
  this.id = "Greenhouse";
  this.name = "Greenhouse";

  var parentElement = null;

  if (!options || !options.hasOwnProperty('url') || typeof options.url !== 'string') {
    throw new Error("cannot make Greenhouse without url");
  }
  var url = options.url;

  this.render = function render(_parentElement, callback) {
    parentElement = _parentElement;
    Reef.renderer.render(parentElement, "modules.common.iframe", {url: url}, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  };

  this.activate = function activate(options, callback) {
    parentElement.show();
    if (typeof callback === 'function') {
      callback();
    }
  };

  this.deactivate = function deactivate(callback) {
    parentElement.hide();
    if (typeof callback === 'function') {
      callback();
    }
  };
};
Reef.modules.Greenhouse.prototype = Object.create(Reef.lib.reef.modules.Module.prototype);
