
Reef.modules.Coderpad = function Coderpad(options) {
  var self = this;
  this.id = "coderpad";
  this.name = "Coderpad";

  var parentElement = null;

  if (!options || !options.hasOwnProperty('url') || typeof options.url !== 'string') {
    console.log("coderpad options is " + JSON.stringify(options));
    throw new Error("cannot make coderpad without url");
  }
  var url = options.url;

  this.render = function render(_parentElement, callback) {
    parentElement = _parentElement;
    Reef.renderer.render(parentElement, "modules.common.iframe", {url: url}, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  };

  this.activate = function activate(options, callback) {
    parentElement.show();
    if (typeof callback === 'function') {
      callback();
    }
  };

  this.deactivate = function deactivate(callback) {
    parentElement.hide();
    if (typeof callback === 'function') {
      callback();
    }
  };
};
Reef.modules.Coderpad.prototype = Object.create(Reef.lib.reef.modules.Module.prototype);
